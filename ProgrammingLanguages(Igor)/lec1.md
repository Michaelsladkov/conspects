# Лекция 1

## Организационные моменты

Будем изучать ситстемы на Си и асме. 
5 Лабораторных работ 2 по асму, 3 по Си. Код-ревью в два этапа.
Обязательный онлайн-курс. Возможен **индивидуальный** проект.

Инструменты:
- Gitlab
- Telegram

Источники:
- Low Level Rrogramming
- Principles of Computer System Design

---

## Суть
### Что такое системы
Система - Набор взаимосвязанных частей, обладающим за счёт связей большими свойствами, чем сумма свойств всех компонентов

Элементы системы - в том числе, другие системы. Системы работают в окружении. 

> В вычислительных системах мы через интерфейс наблюдаем процессы внутри и интерпретируем результаты - так и проходят вычисления

### Программы как системы
Исходный код - не система.

Выполняющаяся программа - часть программно-аппаратной вычислительной системы

## Декомпозиция систем
Способы декомпозиции систем:
- **Композиционный**: составные части
- **Функциональный**: реализуемые частями функции

Функциональная декмпозиция работающей над проектом группы: роли членов команды (прогер, дизайнер ...)

## Сложность систем
- Сложно описать
- Сложно создать

Признаки сложной системы:
1. Много частей
2. Много связей
3. Много нерегулярностей (исключений из правил)
4. Большое описание
5. Поддерживается большой командой

Типы функциональных компонентов вычислительных систем:
1. Исполнитель (interpreter) - реакция на события
2. Память - хранение
3. Транспорт (communication link) - связь между компонентами



### Вопросы Игорю
1. Где найти LLP
1. Как лучше учить французский